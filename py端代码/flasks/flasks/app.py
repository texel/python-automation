"""
This script runs the application using a development server.
It contains the definition of routes and views for the application.
"""

from flask import Flask,request,render_template,jsonify
import automation
import time
app = Flask(__name__)

# Make the WSGI interface available at the top level so wfastcgi can get it.
wsgi_app = app.wsgi_app


@app.route('/')
def hello():
    """Renders a sample page."""
    return "Hello World!"


@app.route('/automation',methods=['POST'])
def automations():
    returnS =""
    dataraw=request.get_json()
    key = dataraw["cycle"][0]
    if key=="1":
        automation.mainWork(dataraw["id"])
    elif key =="2":
        while True:
            automation.mainWork(dataraw["id"])
            time.sleep(0.1)
    return returnS

if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    app.run(HOST, PORT)
