import pyautogui
import time
import pyperclip
import EasySqlite
import os
import  sys
#定义鼠标事件

#pyautogui库其他用法 https://blog.csdn.net/qingfengxd1/article/details/108270159

def mouseClick(clickTimes,lOrR,img,reTry):
    try:
        if reTry == 1:
            while True:
                #识别图片
                location=pyautogui.locateCenterOnScreen(img,confidence=0.9)
                if location is not None:
                    # 获取x,y轴后点击
                    pyautogui.click(location.x,location.y,clicks=clickTimes,interval=0.2,duration=0.2,button=lOrR)
                    break
                print("未找到匹配图片,0.1秒后重试")
                time.sleep(0.1)
        elif reTry == -1:
            while True:
                location=pyautogui.locateCenterOnScreen(img,confidence=0.9)
                if location is not None:
                    pyautogui.click(location.x,location.y,clicks=clickTimes,interval=0.2,duration=0.2,button=lOrR)
                time.sleep(0.1)
        elif reTry > 1:
            i = 1
            while i < reTry + 1:
                location=pyautogui.locateCenterOnScreen(img,confidence=0.9)
                if location is not None:
                    pyautogui.click(location.x,location.y,clicks=clickTimes,interval=0.2,duration=0.2,button=lOrR)
                    print("重复")
                    i += 1
                time.sleep(0.1)
    except Exception as r:
        print(r)
        return  r
#任务
def mainWork(id):
    try:
        db = EasySqlite.EasySqlite('RAP.db')
        where = f"WHERE The_serial_number = '{id}' Order By xh "
        date = db.execute(F"SELECT * from configuration {where}")
        for i in date:
            instruction = i["instruction"][0]
            content = i["content"]
            #取本行指令的操作类型
            cmdType = instruction
            if cmdType == "1":
                #取图片名称
                img = content
                reTry = 1
                mouseClick(1,"left",img,reTry)
                print("单击左键",img)
            #2代表双击左键
            elif cmdType == "2":
                #取图片名称
                img = content
                #取重试次数
                reTry = 1
                mouseClick(2,"left",img,reTry)
                print("双击左键",img)
            #3代表右键
            elif cmdType == "3":
                #取图片名称
                img =content
                #取重试次数
                reTry = 1
                mouseClick(1,"right",img,reTry)
                print("右键",img)
            #4代表输入
            elif cmdType == "4":
                inputValue = content
                pyperclip.copy(inputValue)
                pyautogui.hotkey('ctrl','v')
                time.sleep(0.5)
                print("输入:",inputValue)
            #5代表等待
            elif cmdType == "5":
                #取图片名称
                waitTime = int(content)
                time.sleep(waitTime)
                print("等待",waitTime,"秒")
            #6代表滚轮
            elif cmdType == "6":
                #取图片名称
                scroll = content
                pyautogui.scroll(int(scroll))
                print("滚轮滑动",int(scroll),"距离")
            elif cmdType == "7":
                # 执行组合键
                hotkey_before =content.split("/")[0]
                hotkey_after = content.split("/")[1]
                print("执行组合键"+content)
                pyautogui.hotkey(hotkey_before, hotkey_after)
                # 8代表_粘贴当前时间
            elif cmdType == "8":
                # 设置本机当前时间。
                localtime = time.strftime("%Y-%m-%d %H：%M：%S", time.localtime())
                pyperclip.copy(localtime)
                pyautogui.hotkey('ctrl', 'v')
                print("粘贴了本机时间:", localtime)
                time.sleep(0.5)
                # 9代表_系统命令集模式
            elif cmdType == "9":
                wincmd = content
                os.system(wincmd)
                print("运行系统命令:", wincmd)
                time.sleep(0.5)

    except Exception as r:
        print(r)
        return r
