﻿namespace ptdy
{
    partial class perform
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(perform));
            this.button1 = new System.Windows.Forms.Button();
            this.task = new System.Windows.Forms.Button();
            this.content = new System.Windows.Forms.TextBox();
            this.Perform_a_task = new System.Windows.Forms.Button();
            this.task_dataGridView = new System.Windows.Forms.DataGridView();
            this.configuration_dataGridView = new System.Windows.Forms.DataGridView();
            this.instruction = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.RWname = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.steps = new System.Windows.Forms.Button();
            this.delete = new System.Windows.Forms.Button();
            this.delete_steps = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.steps_id = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.bz = new System.Windows.Forms.TextBox();
            this.update__steps = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.url_text = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.task_dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.configuration_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(198, 226);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "添加图片";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // task
            // 
            this.task.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.task.Location = new System.Drawing.Point(14, 54);
            this.task.Name = "task";
            this.task.Size = new System.Drawing.Size(79, 50);
            this.task.TabIndex = 1;
            this.task.Text = "添加任务";
            this.task.UseVisualStyleBackColor = false;
            this.task.Click += new System.EventHandler(this.button2_Click);
            // 
            // content
            // 
            this.content.Location = new System.Drawing.Point(71, 226);
            this.content.Name = "content";
            this.content.Size = new System.Drawing.Size(121, 21);
            this.content.TabIndex = 3;
            // 
            // Perform_a_task
            // 
            this.Perform_a_task.BackColor = System.Drawing.Color.SpringGreen;
            this.Perform_a_task.Location = new System.Drawing.Point(198, 54);
            this.Perform_a_task.Name = "Perform_a_task";
            this.Perform_a_task.Size = new System.Drawing.Size(89, 50);
            this.Perform_a_task.TabIndex = 5;
            this.Perform_a_task.Text = "执行任务";
            this.Perform_a_task.UseVisualStyleBackColor = false;
            this.Perform_a_task.Click += new System.EventHandler(this.Perform_a_task_Click);
            // 
            // task_dataGridView
            // 
            this.task_dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.task_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.task_dataGridView.Location = new System.Drawing.Point(14, 282);
            this.task_dataGridView.Name = "task_dataGridView";
            this.task_dataGridView.RowTemplate.Height = 23;
            this.task_dataGridView.Size = new System.Drawing.Size(375, 151);
            this.task_dataGridView.TabIndex = 6;
            this.task_dataGridView.Click += new System.EventHandler(this.tasks);
            // 
            // configuration_dataGridView
            // 
            this.configuration_dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.configuration_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.configuration_dataGridView.Location = new System.Drawing.Point(14, 439);
            this.configuration_dataGridView.Name = "configuration_dataGridView";
            this.configuration_dataGridView.RowTemplate.Height = 23;
            this.configuration_dataGridView.Size = new System.Drawing.Size(375, 143);
            this.configuration_dataGridView.TabIndex = 7;
            this.configuration_dataGridView.Click += new System.EventHandler(this.delete_stepsss);
            // 
            // instruction
            // 
            this.instruction.AutoSize = true;
            this.instruction.Location = new System.Drawing.Point(12, 198);
            this.instruction.Name = "instruction";
            this.instruction.Size = new System.Drawing.Size(53, 12);
            this.instruction.TabIndex = 8;
            this.instruction.Text = "指令类型";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "1-单击",
            "2-双击",
            "3-右键",
            "4-输入",
            "5-等待",
            "6-滚轮",
            "7-热键组合",
            "8-粘贴当前时间",
            "9-windows CMD命令模式"});
            this.comboBox1.Location = new System.Drawing.Point(71, 195);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(297, 20);
            this.comboBox1.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 235);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "执行内容";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 11;
            this.label2.Text = "任务名称";
            // 
            // RWname
            // 
            this.RWname.Location = new System.Drawing.Point(71, 119);
            this.RWname.Name = "RWname";
            this.RWname.Size = new System.Drawing.Size(297, 21);
            this.RWname.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(14, 588);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(375, 34);
            this.button2.TabIndex = 13;
            this.button2.Text = "作者信息/使用手册";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // steps
            // 
            this.steps.Location = new System.Drawing.Point(14, 253);
            this.steps.Name = "steps";
            this.steps.Size = new System.Drawing.Size(105, 23);
            this.steps.TabIndex = 14;
            this.steps.Text = "添加步骤";
            this.steps.UseVisualStyleBackColor = true;
            this.steps.Click += new System.EventHandler(this.steps_Click);
            // 
            // delete
            // 
            this.delete.BackColor = System.Drawing.Color.OrangeRed;
            this.delete.Location = new System.Drawing.Point(113, 54);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(79, 50);
            this.delete.TabIndex = 15;
            this.delete.Text = "删除任务";
            this.delete.UseVisualStyleBackColor = false;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // delete_steps
            // 
            this.delete_steps.Location = new System.Drawing.Point(236, 253);
            this.delete_steps.Name = "delete_steps";
            this.delete_steps.Size = new System.Drawing.Size(105, 23);
            this.delete_steps.TabIndex = 16;
            this.delete_steps.Text = "删除步骤";
            this.delete_steps.UseVisualStyleBackColor = true;
            this.delete_steps.Click += new System.EventHandler(this.delete_steps_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 17;
            this.label3.Text = "步骤序号";
            // 
            // steps_id
            // 
            this.steps_id.Location = new System.Drawing.Point(71, 172);
            this.steps_id.Name = "steps_id";
            this.steps_id.Size = new System.Drawing.Size(297, 21);
            this.steps_id.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 19;
            this.label4.Text = "任务备注";
            // 
            // bz
            // 
            this.bz.Location = new System.Drawing.Point(71, 145);
            this.bz.Name = "bz";
            this.bz.Size = new System.Drawing.Size(297, 21);
            this.bz.TabIndex = 20;
            // 
            // update__steps
            // 
            this.update__steps.Location = new System.Drawing.Point(125, 253);
            this.update__steps.Name = "update__steps";
            this.update__steps.Size = new System.Drawing.Size(105, 23);
            this.update__steps.TabIndex = 21;
            this.update__steps.Text = "修改步骤";
            this.update__steps.UseVisualStyleBackColor = true;
            this.update__steps.Click += new System.EventHandler(this.update__steps_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 22;
            this.label5.Text = "启动接口";
            // 
            // url_text
            // 
            this.url_text.Location = new System.Drawing.Point(71, 18);
            this.url_text.Name = "url_text";
            this.url_text.Size = new System.Drawing.Size(216, 21);
            this.url_text.TabIndex = 23;
            this.url_text.Text = " http://localhost:5555/automation";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "1-执行一次",
            "2-循环执行"});
            this.comboBox2.Location = new System.Drawing.Point(285, 70);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(110, 20);
            this.comboBox2.TabIndex = 24;
            this.comboBox2.Text = "选择执行次数";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(294, 18);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 25;
            this.button3.Text = "启动服务";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // perform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 634);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.url_text);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.update__steps);
            this.Controls.Add(this.bz);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.steps_id);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.delete_steps);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.steps);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.RWname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.instruction);
            this.Controls.Add(this.configuration_dataGridView);
            this.Controls.Add(this.task_dataGridView);
            this.Controls.Add(this.Perform_a_task);
            this.Controls.Add(this.content);
            this.Controls.Add(this.task);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "perform";
            this.Text = "自动化程序";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.task_dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.configuration_dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button task;
        private System.Windows.Forms.TextBox content;
        private System.Windows.Forms.Button Perform_a_task;
        private System.Windows.Forms.DataGridView task_dataGridView;
        private System.Windows.Forms.DataGridView configuration_dataGridView;
        private System.Windows.Forms.Label instruction;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox RWname;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button steps;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button delete_steps;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox steps_id;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox bz;
        private System.Windows.Forms.Button update__steps;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox url_text;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button button3;
    }
}

