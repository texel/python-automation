﻿using System;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace ptdy
{
    public partial class perform : Form
    {
        SQLiteHelper sh = SQLiteHelper.Instance;
        string sql;
        string id;
        string names;
        string xqid;
        string uuid;
        public perform()
        {
            InitializeComponent();

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            sh.Open("RAP.db");
            select();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pyexePath = "";
            // string a = textBox3.Text;
            string b = content.Text;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;//该值确定是否可以选择多个文件
            dialog.Title = "请选择文件夹";
            dialog.Filter = "所有文件(*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                pyexePath = dialog.FileName;
            }
            content.Text = pyexePath;
        }


        private void button2_Click(object sender, EventArgs e)
        {

            string sj = DateTime.Now.ToLocalTime().ToString();
            if (uuid == null)
            {
                uuid = System.Guid.NewGuid().ToString("N");
            }
            sql = $"INSERT INTO task (id, name, note,time)VALUES ('{uuid}', '{RWname.Text}', '{bz.Text}','{sj}');";

            string fh = sh.Insert(sql);
            if (fh == "插入成功")
            {
                MessageBox.Show("插入成功");
                select();
                uuid = System.Guid.NewGuid().ToString("N");
            }
            else
            {
                MessageBox.Show(fh);
            }

        }

        public void select()
        {
            try
            {
                sql = "select name  任务名称, note  备注,time  任务建立时间 ,id  from task Order By time Desc";
                DataTable data = sh.Select(sql);
                task_dataGridView.DataSource = data;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void select_xq(string xh)
        {
            sql = $"SELECT  xh 序号, instruction 指令,content 内容 ,id  FROM configuration WHERE The_serial_number = '{xh}' Order By xh ";
            DataTable data = sh.Select(sql);
            configuration_dataGridView.DataSource = data;
        }
        private void Perform_a_task_Click(object sender, EventArgs e)
        {
            if (comboBox2.Text=="选择执行次数")
            {
                MessageBox.Show("需要选择执行次数才可以执行！！！！");
            }
            else if (id==null)
            {
                MessageBox.Show("没有选择任务ID，请选择任务ID！！！！");
            }
            else
            {
                string url = url_text.Text;
                var abcObject = new
                {
                    id = id,
                    cycle = comboBox2.Text
                };
                string serJson = JsonConvert.SerializeObject(abcObject);
                Post(url, serJson);
                MessageBox.Show("脚本执行成功！！");
            }

        }

        private void tasks(object sender, EventArgs e)
        {
            try
            {
                id = task_dataGridView.SelectedRows[0].Cells["id"].Value.ToString();
                string names = task_dataGridView.SelectedRows[0].Cells["任务名称"].Value.ToString();
                RWname.Text =names;
                select_xq(id);
            }
            catch
            {
                MessageBox.Show("点击ID前方空白处选择任务号!!!");
            }
        }

        private void delete_Click(object sender, EventArgs e)
        {
            MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
            DialogResult dr = MessageBox.Show($"是否删除 { RWname.Text }任务?", "删除任务", messButton);
            if (dr == DialogResult.OK)
            {
                if (label2.Text==null||id !=null)
                {
                    sql = $"DELETE FROM task WHERE ID = '{id}'";
                    string rt = sh.delete(sql);
                    sql =$"DELETE FROM configuration WHERE The_serial_number ='{id}'";
                    sh.delete(sql);
                    if (rt == "删除成功！")
                    {
                        select();
                        MessageBox.Show(rt);
                        RWname.Text = null;
                    }
                }
            }
        }

        private void steps_Click(object sender, EventArgs e)
        {
            if (id==null)
            {
                MessageBox.Show("任务ID为空添加任务详情失败");
            }
            if (uuid == null)
            {
                uuid = Guid.NewGuid().ToString("N");
            }
            else
            {
                sql ="INSERT INTO configuration(id, The_serial_number, instruction, content, repeats, xh) " +
                $"VALUES ('{uuid}', '{id}', '{comboBox1.Text}', '{content.Text}', ' ', '{steps_id.Text}');";
                string fh = sh.Insert(sql);
                if (fh == "插入成功")
                {
                    MessageBox.Show("插入成功");
                    select();
                    uuid = Guid.NewGuid().ToString("N");

                }
                else
                {
                    MessageBox.Show(fh);
                }
            }

        }

        private void delete_steps_Click(object sender, EventArgs e)
        {
            MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
            DialogResult dr = MessageBox.Show($"是否删除 { names } ?", "删除任务", messButton);
            if (dr == DialogResult.OK)
            {
                sql =$"DELETE FROM configuration WHERE id ='{xqid}'";
                string rt = sh.delete(sql);
                if (rt == "删除成功！")
                {
                    select();
                    MessageBox.Show(rt);
                    RWname.Text = null;
                }
            }

        }

        private void delete_stepsss(object sender, EventArgs e)
        {
            try
            {
                xqid = configuration_dataGridView.SelectedRows[0].Cells["id"].Value.ToString();
                names = configuration_dataGridView.SelectedRows[0].Cells["指令"].Value.ToString();
                comboBox1.Text = names;
                steps_id.Text = configuration_dataGridView.SelectedRows[0].Cells["序号"].Value.ToString();
                content.Text =  configuration_dataGridView.SelectedRows[0].Cells["内容"].Value.ToString();

            }
            catch
            {

            }

        }

        private void update__steps_Click(object sender, EventArgs e)
        {
            sql = $"UPDATE configuration SET instruction = '{comboBox1.Text}',content='{content.Text}' ,xh = '{steps_id.Text}' WHERE id = '{xqid}' ";
            sh.Update(sql);
            MessageBox.Show("修改成功！！！");
            select_xq(xqid);
        }


        #region POST-JSON  
        /// <summary>
        /// 发送post
        /// </summary>
        /// <param name="requestUri"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public static string Post(string requestUri, string json)
        {
            string strUri = requestUri;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(strUri);
            request.Method = "POST";
            request.ContentType = "application/json";
            string paraUriCoded = json;
            byte[] payload;
            payload = System.Text.Encoding.UTF8.GetBytes(paraUriCoded);
            request.ContentLength = payload.Length;
            Stream writer;
            try
            {
                writer = request.GetRequestStream();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            writer.Write(payload, 0, payload.Length);
            writer.Close();
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                response = ex.Response as HttpWebResponse;
            }
            Stream s = response.GetResponseStream();
            StreamReader sRead = new StreamReader(s);
            string postContent = sRead.ReadToEnd().ToString();
            sRead.Close();
            return postContent;
        }






        #endregion

        private void button2_Click_1(object sender, EventArgs e)
        {
            The_author the_Author = new The_author();
            the_Author.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Process p = Process.Start("app.exe");
        }
    }
}
