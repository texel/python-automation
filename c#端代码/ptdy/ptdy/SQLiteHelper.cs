﻿//////////////////////////////////////////////////
/// 文件：SQLiteHelper.cs
/// 说明：SQLite管理助手
/// 作者：李光强
/// 时间：2020/11/6/
/// 来源：http://sh.codeplex.com
/////////////////////////////////////////////////

using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace System.Data.SQLite
{
    /// <summary>
    /// 字段类型
    /// </summary>
    public enum FieldType
    {
        Text,
        DateTime,
        Integer,
        Decimal,
        BLOB
    }

    /// <summary>
    /// SQLite管理助手
    /// </summary>
    public class SQLiteHelper
    {
        #region 实例及构造函数
        private static SQLiteHelper instance;

        /// <summary>
        /// 实例
        /// </summary>
        public static SQLiteHelper Instance
        {
            get
            {
                if (instance == null)
                    instance = new SQLiteHelper();

                return instance;
            }
        }


        public SQLiteHelper() {
            //将SQLite目录添加环境变量中
            string mod_spatialite_folderPath = @".\SQLite";
            //using relative path, cannot use absolute path, dll load will fail
            //string mod_spatialite_dllPath = @"mod_spatialite-4.3.0a-win-amd64\mod_spatialite";            
            string path = Environment.GetEnvironmentVariable("Path", EnvironmentVariableTarget.Machine) + ";" + mod_spatialite_folderPath;
            Environment.SetEnvironmentVariable("Path", path, EnvironmentVariableTarget.Process);

        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="command"></param>
        public SQLiteHelper(SQLiteCommand command)
        {
            cmd = command;
        }

        #endregion

        #region 私有变量及属性
        private string db;
        private SQLiteCommand cmd = null;
        private SQLiteConnection conn;

        /// <summary>
        /// 是否打开
        /// </summary>
        private bool isOpened = false;

        /// <summary>
        /// SQLite Command
        /// </summary>
        public SQLiteCommand Command { get => cmd; set => cmd = value; }

        /// <summary>
        /// SQLite Connection
        /// </summary>
        public SQLiteConnection Connection { get => conn; set => conn = value; }

        /// <summary>
        /// 是否打开
        /// </summary>
        public bool IsOpened { get => isOpened; }
        #endregion

        #region 连接管理
        /// <summary>
        /// 打开连接
        /// </summary>
        /// <returns></returns>
        /// <remarks>必须先使用open(databasePath)打开过</remarks>
        public bool Open()
        {
            try
            {
                string dataSource = string.Format("data source={0};Pooling=true;FailIfMissing=false", this.db);
                this.conn = new SQLiteConnection(dataSource);
                conn.Open();

                this.cmd = new SQLiteCommand();
                cmd.Connection = conn;
                isOpened = true;

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 打开连接
        /// </summary>
        /// <param name="databasePath">数据库文件路径</param>
        /// <returns></returns>
        /// <remarks>如果文件不存在则创建数据库</remarks>
        public bool Open(string databasePath)
        {
            try
            {
                this.db = databasePath;
                string dataSource = string.Format("data source={0};Pooling=true;FailIfMissing=false", databasePath);
                this.conn = new SQLiteConnection(dataSource);
                conn.Open();

                this.cmd = new SQLiteCommand();                  
                cmd.Connection = conn;
                isOpened = true;

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 初始化空间参数
        /// </summary>
        /// <remarks>初始创建空间数据库时，必须执行初始化操作</remarks>
        public void InitSpatialMeta()
        {
            try
            {
                //
                string sql = "select InitSpatialMetaData()";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 关闭
        /// </summary>
        public void Close()
        {
            try
            {
                if (conn != null && isOpened)
                {                    
                    conn.Close();
                    isOpened = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
        }
        #endregion

        #region DB Info

        /// <summary>
        /// 是否存在数据表
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ExistsTable(string name)
        {
            DataTable dt = GetTableStatus();
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string t = dt.Rows[i]["name"] + "";
                if (t.ToLower().Trim() == name.ToLower().Trim()) return true;
            }

            return false;
        }

        /// <summary>
        /// 获取数据表状态
        /// </summary>
        /// <returns>DataTable</returns>
        /// <remarks>字段包括：type,name,tbl_name,rootpage,sql</remarks>
        public DataTable GetTableStatus()
        {
            return Select("SELECT * FROM sqlite_master;");
        }

        /// <summary>
        /// 获取数据表列表
        /// </summary>
        /// <returns>DataTable</returns>
        /// <remarks>字段包括：tablename</remarks>
        public DataTable GetTableList()
        {
            DataTable dt = GetTableStatus();
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("tablename");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string t = dt.Rows[i]["name"] + "";
                if (t != "sqlite_sequence")
                    dt2.Rows.Add(t);
            }
            return dt2;
        }

        /// <summary>
        /// 获取数据表字段状态
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns>DataTable</returns>
        /// <remarks>字段包括：cid,name,notnull,dflt_value,pk</remarks>
        public DataTable GetColumnStatus(string tableName)
        {
            return Select(string.Format("PRAGMA table_info(`{0}`);", tableName));
        }

        /// <summary>
        /// 显示数据库信息
        /// </summary>
        /// <returns>DataTable</returns>
        /// <remarks>字段包括：seq,name,file</remarks>
        public DataTable ShowDatabase()
        {
            return Select("PRAGMA database_list;");
        }

        #endregion

        #region 数据查询

        /// <summary>
        /// 开始事务
        /// </summary>
        public void BeginTransaction()
        {
            cmd.CommandText = "begin transaction;";
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 提交
        /// </summary>
        public void Commit()
        {
            cmd.CommandText = "commit;";
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 回滚
        /// </summary>
        public void Rollback()
        {
            cmd.CommandText = "rollback";
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 查询SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable Select(string sql)
        {
            return Select(sql, new List<SQLiteParameter>());
        }

        /// <summary>
        /// 带参数查询
        /// </summary>
        /// <param name="sql">SQL语句</param>
        /// <param name="dicParameters">参数列表</param>
        /// <returns>DataTable</returns>
        /// <remarks>示例：
        /// 
        /// 
        /// </remarks>
        public DataTable Select(string sql, Dictionary<string, object> dicParameters = null)
        {
            List<SQLiteParameter> lst = GetParametersList(dicParameters);
            return Select(sql, lst);
        }

        /// <summary>
        /// 带参数查询
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable Select(string sql, IEnumerable<SQLiteParameter> parameters = null)
        {
            cmd.CommandText = sql;
            if (parameters != null)
            {
                foreach (var param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
            }
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="table">数据表名</param>
        /// <param name="where">条件(不含where）</param>
        /// <param name="order">排序字段(不含order by)</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="pageIndex">页号(从0开始)</param>
        /// <returns></returns>
        public DataTable SelectByPage(string table,string where,string order,int pageSize,int pageIndex)
        {
            //select * from LidarT where T>0 order by T limit 10 offset 10;
            cmd.CommandText =
                $"select * from {table}";
            if (!string.IsNullOrEmpty(where))
                cmd.CommandText += $" where {where}";

            if (!string.IsNullOrEmpty(order))
                cmd.CommandText += $" order by {order}";

            cmd.CommandText+= $" limit {pageSize} offset {pageSize*pageIndex}";

            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        /// <summary>
        /// 获取分页数量
        /// </summary>
        /// <param name="table">数据表名</param>
        /// <param name="where">查询条件（不含where)</param>
        /// <param name="pageSize">页面大小</param>
        /// <returns></returns>
        public int PageCount(string table, string where,int pageSize)
        {
            long rs = this.GetRecordsCount(table, where);
            return (int)Math.Ceiling(((Single)rs / pageSize));
        }

        /// <summary>
        /// 检查数据是否存在
        /// </summary>
        /// <param name="table">数据表名</param>
        /// <param name="where">where条件（不含where关键字)</param>
        /// <returns></returns>
        public bool Exists(string table, string where)
        {
            string sql = string.Format("select count(*) from {0} where {1}", table, where);
            DataTable dt = this.Select(sql);
            return int.Parse(dt.Rows[0][0].ToString()) > 0;
        }

        /// <summary>
        /// 获取记录数量
        /// </summary>
        /// <param name="table">数据表名</param>
        /// <param name="where">where条件（不含where关键字)</param>
        /// <returns></returns>
        public long GetRecordsCount(string table, string where)
        {
            string sql = string.Format("select count(*) from {0}", table);
            if(!string.IsNullOrEmpty(where))
                sql+=$" where {where}";
            DataTable dt = this.Select(sql);
            return long.Parse(dt.Rows[0][0].ToString());
        }

        /// <summary>
        /// 执行SQL
        /// </summary>
        /// <param name="sql"></param>
        public void Execute(string sql)
        {
            Execute(sql, new List<SQLiteParameter>());
        }

        /// <summary>
        /// 执行带参数SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="dicParameters"></param>
        public void Execute(string sql, Dictionary<string, object> dicParameters = null)
        {
            List<SQLiteParameter> lst = GetParametersList(dicParameters);
            Execute(sql, lst);
        }

        /// <summary>
        /// 执行带参数SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        public void Execute(string sql, IEnumerable<SQLiteParameter> parameters = null)
        {
            cmd.CommandText = sql;
            if (parameters != null)
            {
                foreach (var param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
            }
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 执行SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public object ExecuteScalar(string sql)
        {
            cmd.CommandText = sql;
            return cmd.ExecuteScalar();
        }

        /// <summary>
        /// 执行带参数SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="dicParameters"></param>
        /// <returns></returns>
        public object ExecuteScalar(string sql, Dictionary<string, object> dicParameters = null)
        {
            List<SQLiteParameter> lst = GetParametersList(dicParameters);
            return ExecuteScalar(sql, lst);
        }

        /// <summary>
        /// 执行带参数SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public object ExecuteScalar(string sql, IEnumerable<SQLiteParameter> parameters = null)
        {
            cmd.CommandText = sql;
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    cmd.Parameters.Add(parameter);
                }
            }
            return cmd.ExecuteScalar();
        }

        public dataType ExecuteScalar<dataType>(string sql, Dictionary<string, object> dicParameters = null)
        {
            List<SQLiteParameter> lst = null;
            if (dicParameters != null)
            {
                lst = new List<SQLiteParameter>();
                foreach (KeyValuePair<string, object> kv in dicParameters)
                {
                    lst.Add(new SQLiteParameter(kv.Key, kv.Value));
                }
            }
            return ExecuteScalar<dataType>(sql, lst);
        }

        public dataType ExecuteScalar<dataType>(string sql, IEnumerable<SQLiteParameter> parameters = null)
        {
            cmd.CommandText = sql;
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    cmd.Parameters.Add(parameter);
                }
            }
            return (dataType)Convert.ChangeType(cmd.ExecuteScalar(), typeof(dataType));
        }

        public dataType ExecuteScalar<dataType>(string sql)
        {
            cmd.CommandText = sql;
            return (dataType)Convert.ChangeType(cmd.ExecuteScalar(), typeof(dataType));
        }

        private List<SQLiteParameter> GetParametersList(Dictionary<string, object> dicParameters)
        {
            List<SQLiteParameter> lst = new List<SQLiteParameter>();
            if (dicParameters != null)
            {
                foreach (KeyValuePair<string, object> kv in dicParameters)
                {
                    lst.Add(new SQLiteParameter(kv.Key, kv.Value));
                }
            }
            return lst;
        }

        public string Escape(string data)
        {
            data = data.Replace("'", "''");
            data = data.Replace("\\", "\\\\");
            return data;
        }

        /// <summary>
        /// 插入数据
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dic"></param>
        public void Insert(string tableName, Dictionary<string, object> dic)
        {
            StringBuilder sbCol = new System.Text.StringBuilder();
            StringBuilder sbVal = new System.Text.StringBuilder();

            foreach (KeyValuePair<string, object> kv in dic)
            {
                if (sbCol.Length == 0)
                {
                    sbCol.Append("insert into ");
                    sbCol.Append(tableName);
                    sbCol.Append("(");
                }
                else
                {
                    sbCol.Append(",");
                }

                sbCol.Append("`");
                sbCol.Append(kv.Key);
                sbCol.Append("`");

                if (sbVal.Length == 0)
                {
                    sbVal.Append(" values(");
                }
                else
                {
                    sbVal.Append(", ");
                }

                sbVal.Append("@v");
                sbVal.Append(kv.Key);
            }

            sbCol.Append(") ");
            sbVal.Append(");");

            cmd.CommandText = sbCol.ToString() + sbVal.ToString();

            foreach (KeyValuePair<string, object> kv in dic)
            {
                cmd.Parameters.AddWithValue("@v" + kv.Key, kv.Value);
            }

            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 插入数据
        /// </summary>
        /// <param name="sql">要插入的SQL语句</param>
        /// <param name="dic"></param>
        public string  Insert(string sql)
        {
            try
            {
                cmd.CommandText =sql;
                cmd.ExecuteNonQuery();
                return "插入成功";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
           
        } 
        
        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="sql">要插入的SQL语句</param>
        /// <param name="dic"></param>
        public void Update(string sql)
        {          
            this.Insert(sql);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public string delete(string sql)
        {
            this.Insert(sql);
            return "删除成功！";

        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dicData"></param>
        /// <param name="colCond"></param>
        /// <param name="varCond"></param>
        public void Update(string tableName, Dictionary<string, object> dicData, string colCond, object varCond)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic[colCond] = varCond;
            Update(tableName, dicData, dic);
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dicData"></param>
        /// <param name="dicCond"></param>
        public void Update(string tableName, Dictionary<string, object> dicData, Dictionary<string, object> dicCond)
        {
            if (dicData.Count == 0)
                throw new Exception("dicData is empty.");

            StringBuilder sbData = new System.Text.StringBuilder();

            Dictionary<string, object> _dicTypeSource = new Dictionary<string, object>();

            foreach (KeyValuePair<string, object> kv1 in dicData)
            {
                _dicTypeSource[kv1.Key] = null;
            }

            foreach (KeyValuePair<string, object> kv2 in dicCond)
            {
                if (!_dicTypeSource.ContainsKey(kv2.Key))
                    _dicTypeSource[kv2.Key] = null;
            }

            sbData.Append("update `");
            sbData.Append(tableName);
            sbData.Append("` set ");

            bool firstRecord = true;

            foreach (KeyValuePair<string, object> kv in dicData)
            {
                if (firstRecord)
                    firstRecord = false;
                else
                    sbData.Append(",");

                sbData.Append("`");
                sbData.Append(kv.Key);
                sbData.Append("` = ");

                sbData.Append("@v");
                sbData.Append(kv.Key);
            }

            sbData.Append(" where ");

            firstRecord = true;

            foreach (KeyValuePair<string, object> kv in dicCond)
            {
                if (firstRecord)
                    firstRecord = false;
                else
                {
                    sbData.Append(" and ");
                }

                sbData.Append("`");
                sbData.Append(kv.Key);
                sbData.Append("` = ");

                sbData.Append("@c");
                sbData.Append(kv.Key);
            }

            sbData.Append(";");

            cmd.CommandText = sbData.ToString();

            foreach (KeyValuePair<string, object> kv in dicData)
            {
                cmd.Parameters.AddWithValue("@v" + kv.Key, kv.Value);
            }

            foreach (KeyValuePair<string, object> kv in dicCond)
            {
                cmd.Parameters.AddWithValue("@c" + kv.Key, kv.Value);
            }

            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 获取最后插入的ID
        /// </summary>
        /// <returns></returns>
        public long LastInsertRowId()
        {
            return ExecuteScalar<long>("select last_insert_rowid();");
        }

        #endregion

        #region 数据表操作

        /// <summary>
        /// 修改表名
        /// </summary>
        /// <param name="tableFrom"></param>
        /// <param name="tableTo"></param>
        public void RenameTable(string tableFrom, string tableTo)
        {
            cmd.CommandText = string.Format("alter table `{0}` rename to `{1}`;", tableFrom, tableTo);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 复制表记录
        /// </summary>
        /// <param name="tableFrom"></param>
        /// <param name="tableTo"></param>
        public void CopyAllData(string tableFrom, string tableTo)
        {
            DataTable dt1 = Select(string.Format("select * from `{0}` where 1 = 2;", tableFrom));
            DataTable dt2 = Select(string.Format("select * from `{0}` where 1 = 2;", tableTo));

            Dictionary<string, bool> dic = new Dictionary<string, bool>();

            foreach (DataColumn dc in dt1.Columns)
            {
                if (dt2.Columns.Contains(dc.ColumnName))
                {
                    if (!dic.ContainsKey(dc.ColumnName))
                    {
                        dic[dc.ColumnName] = true;
                    }
                }
            }

            foreach (DataColumn dc in dt2.Columns)
            {
                if (dt1.Columns.Contains(dc.ColumnName))
                {
                    if (!dic.ContainsKey(dc.ColumnName))
                    {
                        dic[dc.ColumnName] = true;
                    }
                }
            }

            StringBuilder sb = new Text.StringBuilder();

            foreach (KeyValuePair<string, bool> kv in dic)
            {
                if (sb.Length > 0)
                    sb.Append(",");

                sb.Append("`");
                sb.Append(kv.Key);
                sb.Append("`");
            }

            StringBuilder sb2 = new Text.StringBuilder();
            sb2.Append("insert into `");
            sb2.Append(tableTo);
            sb2.Append("`(");
            sb2.Append(sb.ToString());
            sb2.Append(") select ");
            sb2.Append(sb.ToString());
            sb2.Append(" from `");
            sb2.Append(tableFrom);
            sb2.Append("`;");

            cmd.CommandText = sb2.ToString();
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// 删除表
        /// </summary>
        /// <param name="table"></param>
        public void DropTable(string table)
        {
            cmd.CommandText = string.Format("drop table if exists `{0}`", table);
            cmd.ExecuteNonQuery();
        }

        public void AttachDatabase(string database, string alias)
        {
            Execute(string.Format("attach '{0}' as {1};", database, alias));
        }

        public void DetachDatabase(string alias)
        {
            Execute(string.Format("detach {0};", alias));
        }

        #endregion

    }
}